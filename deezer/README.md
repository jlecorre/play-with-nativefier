# Build a Deezer application by using Nativefier project

## Goals

Transform the Deezer website in a native looking web application using Electron and Docker container!

## How to build Deezer application

```bash
cd deezer/

docker container run \
  -ti --rm --name deezer \
  -v $(pwd)/icon:/src -v $(pwd)/target:/target nativefier/nativefier https://deezer.com/ /target/ \
  --arch x64 \
  --icon /src/deezer-logo-circle-300x300.png \
  --platform linux \
  --single-instance --tray \
  --name deezer
```

And that's all!

## Configuration

The generated executable can ve found in the `target` folder. You can move it where you want to use it.  
As Nativefier cannot know where you're going to move the app, create a `desktop` file. To do this, please refer to the next instructions.

Create a symbolic link of the `deezer` folder under the following path: `$HOME/.local/bin/`:

```bash
ln -s $(pwd)/../deezer $HOME/.local/bin/deezer
```

Then, create a symbolic link of the `desktop` file to the `$HOME/.local/share/applications/` directory:

```bash
ln -s $(pwd)/deezer.desktop ${HOME}/.local/share/applications/deezer.desktop
```

And add the following lines in your `.bashrc` file:

```bash
# Alias related to the Nativefier Deezer webapp
alias deezer='$HOME/.local/bin/deezer/deezer-linux-x64/deezer & > /dev/null 2>&1'
```

If the `desktop` file is not properly loaded, don't hesitate to adapt it to your configuration.

Enjoy!
