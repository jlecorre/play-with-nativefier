# Playing around Nativefier project

## Goals

Transform a website in a native looking web application using Electron and Docker container!

## Source

Nativefier project: <https://hub.docker.com/r/nativefier/nativefier>
Nativefier Docker Hub project: <https://github.com/nativefier/nativefier/>

## Deezer application

By using Docker container, this little project is able to build an Electron application which allowed us to run locally Deezer without using a browser.

More details can be found here: [deezer folder](deezer/README.md).
